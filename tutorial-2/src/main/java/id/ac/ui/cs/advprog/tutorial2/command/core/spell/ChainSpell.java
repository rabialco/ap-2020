package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> listSpell;
    public ChainSpell(ArrayList<Spell> inputSpell){
        this.listSpell = inputSpell;
    }

    @Override
    public void cast() {
        for(Spell spells : listSpell){
            spells.cast();
        }
    }

    @Override
    public void undo() {
        int sizeOflistSpell = listSpell.size();
        for(int i = sizeOflistSpell-1 ; i > -1; i--){
            listSpell.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
