package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //ToDo: Complete me
    public String attack() {
        return "Attack Behavior : Abrakadabra! (Magic Sound)";
    }

    public String getType() {
        return "Attack Type : Magic";
    }
}
