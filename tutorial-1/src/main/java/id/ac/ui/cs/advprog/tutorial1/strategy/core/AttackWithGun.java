package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
        //ToDo: Complete me

    public String attack() {
        return "Attack Behavior : Bang Bang! (Gun Sound)";
    }

    public String getType(){
        return "Attack Type : Gun";
    }
}
