package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
        //ToDo: Complete me
    public String defend(){
        return "Defense Behavior : The Attack are being Defended by Barrier!";
    }

    public String getType(){
        return "Defense Type : Barrier";
    }
}
