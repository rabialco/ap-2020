package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
        //ToDo: Complete me
    public String defend(){
        return "Defense Behavior : The Attack are being Defended by Shield!";
    }

    public String getType(){
        return "Defense Type : Shield";
    }
}
