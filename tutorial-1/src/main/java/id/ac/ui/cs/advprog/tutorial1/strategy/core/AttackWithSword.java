package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    //ToDo: Complete me
    public String attack() {
        return "Attack Behavior : Sheeng Sheeng! (Sword Sound)";
    }

    public String getType() {
        return "Attack Type : Sword";
    }
}
