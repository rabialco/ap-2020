package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
        //ToDo: Complete me
    public String defend(){
        return "Defense Behavior : The Attack are being Defended by Armor!";
    }

    public String getType(){
        return "Defense Type : Armor";
    }
}
